#!/usr/bin/env python3
import sys, os
import numpy as np
import matplotlib.pyplot as plt
import scipy.constants as constants
import scipy.optimize as optimize
import h5py

# H- beam properties, 
m_hminus = 939.29430810e-3

freq = 162.5e6

# design volocity from beam-parameters-at-btl-input
#beta = 0.848069255 # 833 MeV
beta = 0.841636676 # 800 MeV
#gamma = 1.887200508 # 833 MeV
gamma = 1.851700391 # 800 MeV

print('beta from beam-parameters file: ', beta)
print('beta from gamma from beam-parameters file: ', np.sqrt(1 - 1/gamma**2))
print()
print('gamma from beam-parameters file: ', gamma)
print('gamma from beta from beam-parameters file: ', np.sqrt(1.0/(1-beta**2)))
print('gamma**2 - beta**2 gamma**2: ', gamma**2 - beta**2 * gamma**2)

beta = np.sqrt(1 - 1/gamma**2)

pref = m_hminus * beta * gamma
eref = m_hminus * gamma
print('eref: ', eref)
print('ke ref: ', eref-m_hminus)
print('pref: ', pref)

# values for each file row
# x [cm] |  x' [rad] | y [cm] |  y' [rad] |  phi [rad] |  ek [MeV]

particles = np.loadtxt('./BTL650CavityDistributions/realistic-distribution-800MeV/distribution-at-foil.ascii', skiprows=2)

# convert to sensible units [m] and [GeV}
particles[:,0] = 0.01 * particles[:, 0]
particles[:,2] = 0.01 * particles[:, 2]
particles[:,5] = 1.0e-3 * particles[:,5]
npart = particles.shape[0]

# make etot and p column
etot = (particles[:,5] + m_hminus).reshape((npart, 1))
momentum = np.sqrt(particles[:,5].reshape((npart,1)) * (etot + m_hminus))
dpop = (momentum/pref-1).reshape((npart, 1))
zpos = -beta*constants.c * particles[:,4].reshape((npart,1))/(2 * np.pi * freq)

print('particles.shape: ', particles.shape)
print('etot.shape: ', etot.shape)
print('momentum.shape: ', momentum.shape)

#particles = np.hstack((particles, etot, zpos, momentum, dpop))

print('mean x: ', particles[:,0].mean(), ', std x: ', particles[:,0].std())
print('mean y: ', particles[:,2].mean(), ', std y: ', particles[:,2].std())
print()
print('mean x\': ', particles[:,1].mean(), ', std x\': ', particles[:,1].std())
print('mean y\': ', particles[:,3].mean(), ', std y\': ', particles[:,3].std())
print()
print('mean z: ', zpos.mean(), ', std z: ', zpos.std())
print('mean dp/p: ', dpop.mean(), ', std dpop: ', dpop.std())
print('mean ke: ', particles[:,5].mean(), ', std ke: ', particles[:,5].std())
print('mean etot: ', etot.mean(), ', std etot: ', etot.std())

# pcov = np.cov(particles, rowvar=False)
# #print('pcov.shape: ', pcov.shape)
# #print('pcov: ')
# #print(pcov)
# #print()
# print('z - dp/p covariance: ', pcov[7, 9])

meanz = zpos.mean()
stdz = zpos.std()
meandpop = dpop.mean()
stddpop = dpop.std()

plt.figure()
plt.title("x -  x'")
plt.plot(particles[:,0], particles[:,1], '.')
plt.xlabel('x [m]')
plt.ylabel("x' [radians]")

plt.figure()
plt.title("y - y'")
plt.plot(particles[:,2], particles[:,3], '.')
plt.xlabel('y [m]')
plt.ylabel("y' [radians]")

plt.figure()
plt.title('x - y')
plt.plot(particles[:,0], particles[:,2], '.')
plt.xlabel('x')
plt.ylabel('y')

plt.figure()
plt.title('phase - dp/p')
plt.plot(particles[:,4], 100*dpop, '.')
plt.xlabel('phase')
plt.ylabel('dp/p [%]')

plt.figure()
plt.title('dpos - dp/p')
plt.plot(zpos, 100*dpop, '.')
plt.gca().set_xlim((-0.1,0.1))
plt.gca().set_ylim((-0.02, 0.02))
plt.xlabel('z [m]')
plt.ylabel('dp/p [%]')

plt.figure()
plt.title('dp/p distribution')
dpopdat, dpopbins, patches = plt.hist(100*dpop, bins=100)

print('size dpopdat: ', dpopdat.shape[0], ', size dpopbins: ', dpopbins.shape[0])

plt.figure()
plt.title('zpos distribution')
zposdat, zposbins, patches = plt.hist(zpos, bins=100, range=(-0.1,0.1))

print('size zposdat: ', zposdat.shape[0], ', size zposbins: ', zposbins.shape[0])

# fit zpos to parabola
# get midpoint of bins
zposlocs = (zposbins[1:] + zposbins[:-1])/2
# fit to a + b(z-zmean) + c/2 (z-zmean)**2
dz = zposbins[3]-zposbins[2]

# accumulate non-zero data to fit
# we have to restrict it to +- 2 sigma
zposfit = np.array([zposdat[i] for i in range(zposdat.shape[0]) if zposdat[i]!= 0 and abs(zposlocs[i]) < 2*stdz ])
zposerr = np.array([np.sqrt(zposdat[i]) for i in range(zposdat.shape[0]) if zposdat[i] != 0 and abs(zposlocs[i]) < 2*stdz])
zposfitloc = np.array([zposlocs[i] for i in range(zposdat.shape[0]) if zposdat[i] != 0 and abs(zposlocs[i]) < 2*stdz])


print('length zposfit: ', zposfit.shape[0])
print('zposfitloc: ', zposfitloc)
print()
print('zposfit: ', zposfit)
print()
print('zposerr: ', zposerr)
print()

zparam = np.zeros(3, dtype='d')
#zparam[0] = zposdat.max()
#zparam[2] = -zposdat.max()/stdz**2
zparam[0] = zposdat[50]
#zparam[1] = (zposdat[51] - zposdat[49])/(zposlocs[51]-zposlocs[49])
#zparam[2] = 0.25*(zposdat[49] - 2*zposdat[50] + zposdat[51])/(zposlocs[51]-zposlocs[49]**2)
#zparam[0] = zparam[0] - zparam[1]*zposdat[50] - 0.5*zparam[2]*zposdat[50]**2

zparam[1] = 0.5*( (zposdat[54]+zposdat[53]) - (zposdat[46]+zposdat[45]) )/(zposlocs[54]-zposlocs[46])

zparam[2] = 0.5*(zposdat[54]+zposdat[53]) + 0.5*(zposdat[46]+zposdat[45]) - (zposdat[49]+zposdat[50])/(4*(zposlocs[51]-zposlocs[48])**2)

zparam[0] = 0.5*(zposdat[49]+zposdat[50]) - 0.5*zparam[1]*(zposlocs[49]+zposlocs[50]) - 0.5*zparam[2]*(zposlocs[49]+zposlocs[50])**2/4

zparam[0] = zposdat[50]
zparam[1] = (zposdat[55]-zposdat[45])/(zposlocs[55]-zposlocs[45])
zparam[2] = (zposdat[55] + zposdat[45] - 2*zposdat[50])/(zposlocs[55]-zposlocs[50])**2


print('zparam initial: ', zparam)

def zposfunc(z, a, b, c):
    r = a + b*z + 0.5*c*z**2
    return r


# print(-0.05, zposfunc(-0.05, zparam[0], zparam[1], zparam[2]))
# print(-0.025, zposfunc(-0.025, zparam[0], zparam[1], zparam[2]))
# print(0.0, zposfunc(0, zparam[0], zparam[1], zparam[2]))
# print(0.025, zposfunc(0.025, zparam[0], zparam[1], zparam[2]))
# print(0.05, zposfunc(0.05, zparam[0], zparam[1], zparam[2]))

popt = optimize.curve_fit(zposfunc, xdata=zposfitloc, ydata=zposfit, p0=zparam, sigma=zposerr)
print('curve fit results: ', popt[0])

plt.figure()
plt.hist(zpos, bins=zposbins, label='input data')
plt.errorbar(x=zposfitloc, y=zposfit, yerr=zposerr, label='jfo particles')
plt.legend(loc='best')
#plt.plot(zposfitloc, np.array([zposfunc(z, zparam[0], zparam[1], zparam[2]) for z in zposfitloc]), label='hand calc')
plt.plot(zposfitloc,  np.array([zposfunc(z, popt[0][0], popt[0][1], popt[0][2]) for z in zposfitloc]), 'g-', label='fit')
plt.legend(loc='best')

particles[:,4:5] = zpos/beta
particles[:,5:6] = dpop

Mcov = np.cov(particles, rowvar=False)
print('covariance:')
print(Mcov)

np.save('covariance.npy', Mcov)

plt.figure()
plt.title('x distribution')
plt.hist(particles[:,0], bins=100, log=True)

plt.figure()
plt.title('xp distribution')
plt.hist(particles[:,1], bins=100, log=True)

plt.figure()
plt.title('y distribution')
plt.hist(particles[:,2], bins=100, log=True)

plt.figure()
plt.title('yp distribution')
plt.hist(particles[:,3], bins=100, log=True)

# write out the particles as a synergia2 compatible particles diagnostics
# hdf5 file
# have to write out
# charge (scalar)
# mass (scalar)
# particles [N, 7]
# pz (scalar)
# rep (scalar)
# s_n  (scalar)
# tlen (scalar)

h5 = h5py.File('twparticles.h5', 'w')
h5['charge'] = 1
h5['mass'] = constants.m_p
h5['pz'] = pref
h5['rep'] = 1
h5['s_n'] = 10.0
h5['tlen'] = 10.0
# now set up particles
npart = particles.shape[0]
# add particle IDs
pids = np.arange(npart, dtype='d').reshape((npart,1))

particles = np.hstack((particles, pids))
h5['particles'] = particles
h5.close()

plt.show()




