#!/usr/bin/env python3
import sys, os
import numpy as np
import matplotlib.pyplot as plt

part_per_file=2

files=["0050", "0150", "0220", "0284"]
#files=["0000", "0004", "0008", "0012"]

xtunesfig = plt.figure()
ytunesfig = plt.figure()
ztunesfig = plt.figure()
figs = [xtunesfig.number, ytunesfig.number, ztunesfig.number]

filetunes = []
laststarts = []
filepids = []
filelasttunestart = []
for f in files:
    t = np.load('tracks_{}_tunes.npy'.format(f), allow_pickle=True)[()]
    filetunes.append(t)
    partkeys = list(t.keys())
    partkeys.sort()
    filepids.append(partkeys[0:part_per_file])
    lasttunestart = t[partkeys[0]][0][-1]
    filelasttunestart.append(lasttunestart)
    print('file ', f, ' plotting particles ', partkeys[0:part_per_file])
    print('last tune start: ', lasttunestart)


maxtunestart = max(filelasttunestart)

nfiles = len(filetunes)

for i in range(nfiles):
    t = filetunes[i]
    for pid in filepids[i]:
        tunestarts = t[pid][0]
        # adjust tunestarts so all files end at the maxtunestart
        thisfilelasttunestart = tunestarts[-1]
        tunestartoffset = maxtunestart - thisfilelasttunestart
        for j in range(len(tunestarts)):
            tunestarts[j] = tunestarts[j] + tunestartoffset

        xtunes = 1 - np.array(t[pid][1])
        ytunes = 1 - np.array(t[pid][2])
        ztunes = t[pid][3]
        print('particle: ', pid, ' first tune start: ', tunestarts[0], ' , last tune start: ', tunestarts[-1])

        plt.figure(xtunesfig.number)
        plt.plot(tunestarts, xtunes, label='particle {} x tune'.format(pid))
        plt.xlabel('turn')
        plt.ylabel('tune')

        plt.figure(ytunesfig.number)
        plt.plot(tunestarts, ytunes, label='particle {} y tune'.format(pid))
        plt.xlabel('turn')
        plt.ylabel('tune')

        plt.figure(ztunesfig.number)
        plt.plot(tunestarts, ztunes, label='particle {} z tune'.format(pid))
        plt.xlabel('turn')
        plt.ylabel('tune')


for f in figs:
    plt.figure(f)
    plt.legend(loc='best')

plt.figure(xtunesfig.number)
plt.savefig('xtune_evolution.eps')

plt.figure(ytunesfig.number)
plt.savefig('ytune_evolution.eps')

plt.figure(ztunesfig.number)
plt.savefig('ztune_evolution.eps')


plt.show()
