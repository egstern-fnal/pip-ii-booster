#!/usr/bin/env python

# numproc variable refers to the number of 16 core nodes that will
# be allocated and run with 16 threads each.

# The local_options file must be named local_opts.py and placed
# in the Synergia2 job manager search path.

from synergia_workflow import options

# Any instance of the Options class will be added as a suboption.
opts = options.Options('local')
#opts.add("ompnumthreads",2,"number of openmp threads")

# Any instance of the Override class will be used to override
#   the defaults of the existing options.
override = options.Override()
override.account = "m4272"
override.numproc = 128
override.procspernode=128
# The location of the setup.sh for your synergia build
override.setupsh="/devel3-cpu/install/bin/setup.sh"
override.template="job_example"
override.resumetemplate="resume_example"
#override.templatepath="full path to template file"
override.queue="debug"
override.walltime="0:10:00"
