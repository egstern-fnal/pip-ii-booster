#!/usr/bin/python3
from __future__ import print_function
import sys, os
import numpy as np
import synergia
import pickle
import re

from booster_fixup2 import *
from write_elems_pickle import write_elems_pickle

import tfs

lattice_dir="pip-ii-booster-madx-files"
lattice_txt = None

# default timeslot for PIP-II should be 1
def read_booster(timeSlot = 1, ORB_scale=1.0, HP_scale=1.0, VP_scale=1.0, flag_DOG03_ON=0, writefile=False):
    global lattice_txt

    # comments from original madx file:
    # ORB_scale               := 1.0;      !scale factor for ORBUMP 1 => 97mm at foil
    # ! nominal range is 1 collapse after painting to 0 (in roughly 50 turns)
    # HP_scale                := 1.0;      !scale factor for horizontal painting 1 => 5mm at foil
    # ! nominal painting range is from 0 to 1
    # VP_scale                := 1.0;      !scale factor for vertical painting 1 => 16mm offset
    # ! nominal painting range from  1 to 0.312
    # ! removal from foil  range from 0.312 to 0
    # flag_DOG03_ON           = 1;  ! used in *.madx below

    lattice_params = ''
    lattice_params = lattice_params + 'ORB_scale := {};\n'.format(ORB_scale)
    lattice_params = lattice_params + 'HP_scale := {};\n'.format(HP_scale)
    lattice_params = lattice_params + 'VP_scale := {};\n'.format(VP_scale)

    lattice_params = lattice_params + 'flag_DOG03_ON = {};\n'.format(flag_DOG03_ON)

    lattice_params = lattice_params + "timeSlot = {:d};\n".format(timeSlot)

    table_row = timeSlot

    correctors = [(1, "DC.dat"), (table_row, "QL.dat"), (table_row, "QS.dat"), (table_row, "SQL.dat"),
                  (table_row, "SQS.dat"), (table_row, "SXL.dat"), (table_row, "SXS.dat"), (table_row, "SSL.dat"),
                  (table_row, "SSS.dat"), (table_row, "qsdqsf.dat")]

    #print('before reading correctors')
    for linenum,fname in correctors:
        tobj = tfs.Tfs(lattice_dir + "/" + fname)
        #print('before reading corrector ', fname, ' linenum ', linenum)
        lattice_params = lattice_params + tobj.define_row_variables(linenum)
        del tobj

    # fortunately the madX parser ignores value commands sprinkled throughout

    # Initial definitions
    f_main = open("booster_PIP2_header.madx", "r")
    lattice_params = lattice_params + f_main.read()
    f_main.close()

    # injection bumps
    f_inj_bumps = open(lattice_dir + "/" + "booster_PIP2_inj_bumps.dat", "r")
    lattice_params = lattice_params + f_inj_bumps.read()
    f_inj_bumps.close()

    # element definitions
    f_ele = open(lattice_dir + "/" + "booster_PIP2.ele", "r")
    # remove 'return statement'
    booster_ele = f_ele.read()
    booster_ele = re.sub('\nreturn;', '', booster_ele)
    lattice_params = lattice_params + booster_ele
    f_ele.close()

    f_seq = open(lattice_dir + "/" + "booster_PIP2_L11_config_2.seq", "r")
    booster_seq = f_seq.read()
    # fix syntax error in sequence file
    booster_seq = re.sub(r'\nBCEL11_UP:(.*),pk2,,\n', r'\nBCEL11_UP:\1,pk2,\n', booster_seq)
    lattice_params = lattice_params + booster_seq
    f_seq.close()

    # use beam statement from madx files
    lattice_params = lattice_params + "beam, particle=proton, pc=pc;\n"

    if writefile:
        fout = open("pip-ii-booster-combined.madx", 'w')
        fout.write(lattice_params)
        fout.close()

    lattice_txt = lattice_params
    return lattice_params

def get_booster_lattice(timeSlot=1, ORB_scale=1.0, HP_scale=1.0, VP_scale=1.0, flag_DOG03_ON=0, writefile=False):
    lattice_txt = read_booster(timeSlot, ORB_scale, HP_scale, VP_scale, flag_DOG03_ON, writefile)  
    #print('type(lattice_txt): ', type(lattice_txt))
    #print('len(lattice_txt): ', len(lattice_txt))
    orig_lattice = synergia.lattice.MadX_reader().get_lattice('booster', lattice_txt)
    lattice = fixup_lattice(orig_lattice, dogs_to_drifts=False)

    return lattice

def write_elem_lengths(lattice, fname):
    f = open(fname, 'w')
    s = 0.0
    for elem in lattice.get_elements():
        print(elem.get_name(),elem.get_type(), s, elem.get_length(), file=f)
        s = s + elem.get_length()
    f.close()

if __name__ == "__main__":
    # determine synergia version based on existence of methods
    lattice_txt = read_booster(timeSlot=1, writefile=True)
    print('read booster lattice: ', len(lattice_txt), ' characters')
    reader = synergia.lattice.MadX_reader()
    #print('parsing the lattice...')
    reader.parse(lattice_txt)
    #print('line names: ', reader.get_line_names())
    orig_lattice = reader.get_lattice('booster')
    sversion = None
    if hasattr(orig_lattice, 'as_lsexpr'):
        sversion = "2"
    elif hasattr(orig_lattice, 'as_json'):
        sversion = "3"
    f = open('booster_orig-'+sversion+'.out', 'w')
    # trickery to allow running from synergia2 or synergia3
    if sversion == '2':
        print(orig_lattice.as_string(), file=f)
    else:
        print(orig_lattice, file=f)
    f.close()
    if sversion == '2':
        synergia.utils.write_lsexpr_file(orig_lattice.as_lsexpr(), 'booster_orig.lsx')
    else:
        f = open('booster_orig.json', 'w')
        print(orig_lattice.as_json(), file=f)
        f.close()

    write_elem_lengths(orig_lattice, 'elem-lens-before-fixup-'+sversion+'.out')
    lattice = fixup_elements(orig_lattice, dogs_to_drifts=False, kill_sept03=False)
    write_elem_lengths(lattice, 'elem-lens-after-fixup-'+sversion+'.out')

    print('read booster lattice, ', len(lattice.get_elements()), ' elements, len: ', lattice.get_length())

    if sversion == '2':
        synergia.utils.write_lsexpr_file(lattice.as_lsexpr(), 'pip-ii-booster-combined-fixed.lsx')
    else:
        f = open('pip-ii-booster-combined-fixed.json', 'w')
        print(lattice.as_json(), file=f)
        f.close()
