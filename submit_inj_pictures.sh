#!/bin/bash
#SBATCH -N 1
#SBATCH -p cpu_gce
#SBATCH -q regular
#SBATCH --mail-user=egstern@fnal.gov
#SBATCH --mail-type=ALL
#SBATCH -t 01:00:00
#SBATCH -A accelsim
#SBATCH --output=pictures-%j.out
#SBATCH --error=pictures-%j.err

. /work1/accelsim/egstern/syn2-devel-pre3/install/bin/setup.sh
srun -n 16 bash /work1/accelsim/egstern/pip-ii-booster/injection_pictures.sh --mpi=pmix
