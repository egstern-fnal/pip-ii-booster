#!/usr/bin/env python3

import sys, os
import synergia

elem_keywords = {
    'drift': ["l"],
    'hkicker': ['l', 'kick', 'hkick', 'calib'],
    'vkicker': ['l', 'kick', 'vkick', 'calib'],
    'kicker' : ['l', 'hkick', 'vkick', 'calib'],
    'marker' : [],
    'collimator' : ['l'],
    'rcollimator' : ['l'],
    'ecollimator' : ['l'],
    'hmonitor' : ['l'],
    'vmonitor' : ['l'],
    'monitor' : ['l'],
    'instrument' : ['l'],
    'quadrupole': ['l', 'k1', 'k1s', 'tilt', 'calib'],
    'sextupole' : ['l', 'k2', 'k2s', 'tilt', 'calib'],
    'octupole' : ['l', 'k3', 'k3s', 'tilt', 'calib'],
    'multipole' : ['lrad', 'tilt'], # multipoles have knl and ksl vector attributes too
    'rfcavity' : ['l', 'freq' , 'harmon', 'lag', 'volt'],
    'sbend' : ['l', 'angle' , 'e1', 'e2', 'k1', 'k2', 'fint', 'fintx', 'hgap', 'tilt'],
    'rbend' : ['l', 'angle' , 'e1', 'e2', 'k1', 'k2', 'fint', 'fintx', 'hgap', 'tilt']
    }
                  
# convert an element to madx format, return as a string
def convert_elem(elem, pos):
    ename = elem.get_name()
    etype = elem.get_type()

    #elem_def = (etype + ", at={:.20g}").format(pos)
    elem_def = etype

    elem_attr = ""
    if etype in elem_keywords:
        elem_keylist = elem_keywords[etype]
        # include attributes
        for k in elem_keylist:
            if elem.has_double_attribute(k):
                elem_attr = elem_attr + ", {}={:.20g}".format(k, elem.get_double_attribute(k))
        # multipoles possible take knl and ksl vector arguments
        if etype == "multipole":
            for ac in ["knl", "ksl"]:
                if elem.has_vector_attribute(ac):
                    va = elem.get_vector_attribute(ac)
                    elem_attr = elem_attr + ", {}=".format(ac)
                    elem_attr = elem_attr + "{"
                    for idx in range(len(va)):
                        if idx != 0:
                            elem_attr = elem_attr + ", "
                        elem_attr = elem_attr + "{:.20g}".format(va[idx])
                    elem_attr = elem_attr + "}"
    else:
        raise RuntimeError('Unknown element type: {}'.format(etype))

    elem_at = ", at={:.20g};".format(pos)
    #elem_def = elem_def + ";"

    return (elem_def, elem_attr, elem_at)

def unroll_lattice(lattice, outfile=sys.stdout, seqname='model'):

    refpart = lattice.get_reference_particle()

    p0 = refpart.get_momentum()
    brho = p0 * 1.0e9/synergia.foundation.pconstants.c

    elem_defs = []
    elem_attrs = []
    elem_ats = []

    # accumulate definitions of elements in order
    s = 0.0
    for elem in lattice.get_elements():
        elen = elem.get_length()
        estart = s
        eend = s+elen

        elem_def, elem_attr, elem_at = convert_elem(elem, s)
        elem_defs.append(elem_def)
        elem_attrs.append(elem_attr)
        elem_ats.append(elem_at)

        # update new starting position
        s = eend

    
    # first define all the elements with a unique generated name
    for enumber in range(len(elem_defs)):
        print('e{:06d}: {}{};'.format(enumber, elem_defs[enumber],elem_attrs[enumber]), file=outfile)
    
    # now  print out the sequence
    print(file=outfile)
    print("{}: sequence, l={:.20g}, refer=entry;".format(seqname, lattice.get_length()), file=outfile)

    for enumber in range(len(elem_defs)):
        print('e{:06d}{}'.format(enumber, elem_ats[enumber]), file=outfile)

    print("endsequence;", file=outfile)
    print(file=outfile)
    print("beam, particle=proton, energy={:.20g};".format(refpart.get_total_energy()),file=outfile)
    return

def test_unroll():
    lattice = synergia.lattice.Lattice("foo", synergia.lattice.MadX_adaptor_map())
    d1 = synergia.lattice.Lattice_element("drift", "drift1")
    d1.set_double_attribute("l", 1.0)

    q1 = synergia.lattice.Lattice_element("quadrupole", "quad1")
    q1.set_double_attribute("l", 2.0)
    q1.set_double_attribute("k1", -0.05)
    q1.set_double_attribute("k1s", 0.007)
    
    mp = synergia.lattice.Lattice_element("multipole", "m1")
    mp.set_vector_attribute("knl", [1.0, 2.0, 3.0])
    mp.set_vector_attribute("ksl", [4.0, 5.0, 6.0, 7.0])

    sb = synergia.lattice.Lattice_element("sbend", "sb")
    sb.set_double_attribute("l", 2.5)
    sb.set_double_attribute("angle", 3.1415926535/6)
    sb.set_double_attribute("e1", 3.1415926535/12)
    sb.set_double_attribute("e2", 3.1415926535/12)

    rf = synergia.lattice.Lattice_element("rfcavity", "rf")
    rf.set_double_attribute("l", 2.718281828)
    rf.set_double_attribute("volt", 0.2)
    rf.set_double_attribute("harmon", 4)
    rf.set_double_attribute("freq", 53000000.0)
    rf.set_double_attribute("lag", 0.5)

    lattice.append(d1)
    lattice.append(q1)
    lattice.append(mp)
    lattice.append(sb)
    lattice.append(rf)

    refpart = synergia.foundation.Reference_particle(1, 1.0, 5.0/3.0)
    lattice.set_reference_particle(refpart)

    unroll_lattice(lattice, sys.stdout, "foo")

if __name__ == "__main__":
    lsx_file = sys.argv[1]
    print('unrolling lattice ', lsx_file)
    lsxpath = os.path.splitext(lsx_file)
    madx_file_name = lsxpath[0] + '.madx'
    outfile = open(madx_file_name, 'w')

    lattice = synergia.lattice.Lattice(synergia.utils.read_lsexpr_file(lsx_file))
    unroll_lattice(lattice, outfile, 'booster')

    
