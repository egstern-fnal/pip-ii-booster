#!/usr/bin/bash

# 569 for injection + 256 afterwards = 826.

#synbeamplot nfparticles_0012.h5 --minh=-2.0e-7 --maxh=2.0e-7 --minv=-2.0e-7 --maxv=2.0e-7 --bins=40 y yp

#nnnn="0000 0001 0002 0003 0004 0005 0006 0007 0008 0009 0010 0011 0012 0013"
#nnnn=`ls particles_????.h5 | sed -e 's/particles_\([0-9]\{4\}\).h5/\1/'`

if env | grep -iq slurm
then
    mpisize=$SLURM_NTASKS
    myrank=$SLURM_PROCID
else
    mpisize=1
    myrank=0
fi


export MPLCONFIGDIR=/work1/accelsim/egstern/.matplotlib

beamplot="/work1/accelsim/egstern/syn2-devel-pre3/install/lib/synergia_tools/beam_plot.py"

myfirst=826
mylast=827

for ((i=myfirst; i<mylast; ++i ))
do
    f=$(printf "%04d" $i)
    filename="inj_particles_${f}.h5"
    echo "file number: $f, plotting file $filename"

# x xp
    XXPLIM="--minh=-2.2e-2 --maxh=2.2e-2 --minv=-2.1e-3 --maxv=2.1e-3"
    MPLBACKEND=Agg python $beamplot $filename  --bins=50 --contour=10 ${XXPLIM} --output=x-xp-$f.png x xp

# y yp
    YYPLIM="--minh=8.3e-2 --maxh=1.24e-1 --minv=-9.9e-4 --maxv=9.9e-4"
    MPLBACKEND=Agg python $beamplot $filename ${YYPLIM} --bins=50 --contour=10 --output=y-yp-$f.png y yp

# x y
    XYLIM="--minh=-2.2e-2 --maxh=2.2e-2 --minv=8.3e-2 --maxv=1.24e-1"
    MPLBACKEND=Agg python $beamplot $filename  --bins=50 $XYLIM --contour=10 --output=x-y-$f.png x y

    ZDPOPLIM="--minh=-2.8 --maxh=2.8 --minv=-2.4e-3 --maxv=2.4e-3"
    MPLBACKEND=Agg python $beamplot $filename ${ZDPOPLIM} --bins=50 --contour=10 --output=z-zp-$f.png z dpop

done
