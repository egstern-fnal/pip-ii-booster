#!/usr/bin/env python3

import sys, os
import numpy as np
import h5py

for f in sys.argv[1:]:
    print('Reading file ', f)
    h5a = h5py.File(f, 'r')
    fparts = os.path.splitext(f)
    outfile = fparts[0]+'_trunc' + fparts[1]
    print('Writing truncated at 2048 file to ', outfile)
    h5b = h5py.File(outfile, 'w')

    h5b['charge'] = h5a.get('charge')[()]
    h5b['mass'] = h5a.get('mass')[()]
    h5b['pz'] = h5a.get('pz')[()]
    h5b['repetition'] = h5a.get('repetition')[()]
    h5b['s'] = h5a.get('s')[()]
    h5b['s_n'] = h5a.get('s_n')[()]

    h5b['track_coords'] = h5a.get('track_coords')[()][:, 0:2048, :]

    h5b.close()
    h5a.close()

    
