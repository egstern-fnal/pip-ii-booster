#!/usr/env/env python3

import sys, os
import numpy as np
import synergia
import pickle


def write_elems_pickle(lattice):

    stepper = synergia.simulation.Independent_stepper_elements(lattice, 1, 1)
    lattice_simulator = stepper.get_lattice_simulator()

    # save element names and types for offline programs
    elems = []
    s = 0.0

    for i in range(len(lattice.get_elements())):
        e = lattice.get_elements()[i]
        eprev = lattice.get_elements()[i-1]
        e_lf = lattice_simulator.get_lattice_functions(e)
        lfsave = {'beta_x': e_lf.beta_x, 'beta_y': e_lf.beta_y, 'alpha_x': e_lf.alpha_x, 'alpha_y': e_lf.alpha_y, 'D_x': e_lf.D_x, 'D_y': e_lf.D_y }
        elems.append((e.get_name(),
                      e.get_type(),
                      s,
                      e.get_length(),
                      eprev.get_name(),
                      lfsave))
        s = s + e.get_length()

        with open('elems.pickle', 'wb') as f:
            pickle.dump(elems, f)

if __name__ == "__main__":
    lattice = synergia.lattice.Lattice(synergia.utils.read_lsexpr_file("tuned_booster.lsx"))
    write_elems_pickle(lattice)
