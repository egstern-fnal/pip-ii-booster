#!/usr/bin/env python3

import sys, os
import numpy as np
import matplotlib.pyplot as plt
import h5py

fileno = int(sys.argv[1])
tunefile='tracks_{:04d}_tunes.npy'.format(fileno)
trkfile = 'tracks_{:04d}.h5'.format(fileno)

tunes = np.load(tunefile, allow_pickle=True)[()]

plt.figure()
for pid in tunes:
    plt.plot(tunes[pid][0], tunes[pid][3], label='particle {}'.format(pid))
plt.xlabel('turn')
plt.ylabel('tune')
plt.legend(loc='best')

h5 = h5py.File(trkfile, 'r')
pz = h5.get('pz')[()][0]
mass= h5.get('mass')[()]
bg = pz/mass
g = np.sqrt(bg**2 + 1)
b = bg/g

plt.figure()
plt.title('initial x position')
plt.hist(h5.get('track_coords')[0, :, 0], 20)

plt.figure()
plt.title('initial z position')
plt.hist(h5.get('track_coords')[0, :, 4]*b, 20)


plt.show()
