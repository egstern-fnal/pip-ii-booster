#!/usr/bin/env python3

from __future__ import print_function
import sys, os
import numpy as np
import synergia
import pickle
import painting
import tfs

DEBUG = False

import booster_lattice2
from booster_fixup2 import *
from write_elems_pickle import write_elems_pickle

import tfs

from pip2_orbit_options import opts

########################################################################


# manage linac injections into the booster
class linac_injector:

    def __init__(self, linac_covariance, booster_freq, linac_freq, seed, commxx):
        self.bunch_covar = linac_covariance
        self.booster_period = 1.0/booster_freq
        self.linac_period = 1.0/linac_freq
        self.linac_time = -self.booster_period/2.0
        self.means = np.zeros(6, 'd')
        self.limits = np.array([6.0, 6.0, 6.0, 6.0, 2.5, 6.0])
        self.commxx = commxx
        self.dist = synergia.foundation.Random_distribution(seed, self.commxx)

        if commxx.get_rank() == 0:
            print("Booster period: ", self.booster_period)
            print("Linac period: ", self.linac_period)

    # determine bunchlet_times for on-momentum injection
    def on_momentum_bunchlet_times(self):
        # keep bunchlets that fall between [-0.7/2, -0.15/2] and [0.15/2, 0.7/2] of the 
        keep_inj = lambda p: ((p>-0.7/2) and (p<-0.15/2)) or ((p>0.15/2) and (p<0.7/2))
        # how many longitudinal bunchlets can fit in this bunch?
        bunchlet_times = []
        # keep times in units of the bunch period
        while self.linac_time < self.booster_period/2:
            if keep_inj(self.linac_time/self.booster_period):
                bunchlet_times.append(self.linac_time)
            # click to next linac bucket
            self.linac_time += self.linac_period

        # we've gone past the current bucket
        self.linac_time -= self.booster_period

        # now add enough linac period to go to 83 booster periods
        for i in range(83):
            # go to next bucket
            while self.linac_time < self.booster_period/2:
                self.linac_time += self.linac_period
            self.linac_time -= self.booster_period

        if self.commxx.get_rank() == 0:
            print("   linac bunchlets: ", len(bunchlet_times), file=logger)
            print("   linac bunchlet times: ", bunchlet_times)

        return bunchlet_times

    # determine bunchlet_times for on-momentum injection
    def off_momentum_bunchlet_times(self):
        # how many longitudinal bunchlets can fit in this bunch?
        # in units of booster periods [-0.5, 0.5] corresponding to
        # phase [-pi, pi]
        bunchlet_times = []
        # keep times in units of the bunch period -.55/2, .55/2
        keep_inj = lambda p: ((p>-0.55/2) and (p<0.55/2))

        while self.linac_time < self.booster_period/2:
            if keep_inj(self.linac_time/self.booster_period):
                bunchlet_times.append(self.linac_time)
            # click to next linac bucket
            self.linac_time += self.linac_period

        # we've gone past the current bucket
        self.linac_time -= self.booster_period

        # now add enough linac period to go to 83 booster periods
        for i in range(83):
            # go to next bucket
            while self.linac_time < self.booster_period/2:
                self.linac_time += self.linac_period
            self.linac_time -= self.booster_period

        if self.commxx.get_rank() == 0:
            print("   linac bunchlets: ", len(bunchlet_times), file=logger)
            print("   linac bunchlet times: ", bunchlet_times)

        return bunchlet_times

    # generate new bunchlet to inject
    def bunchlet(self, refpart, num_particles, real_charge, logger):

        if opts.on_momentum:
            bunchlet_times = self.on_momentum_bunchlet_times()
        else:
            bunchlet_times = self.off_momentum_bunchlet_times()

        # Now I can generate the correct number of linac pulses
        bunch = synergia.bunch.Bunch(refpart, 0, 0.0, self.commxx)

        for btime in bunchlet_times:
            newbunch = synergia.bunch.Bunch(refpart, num_particles, real_charge, self.commxx)
            
            if opts.linac_beam == "realistic":
                synergia.bunch.populate_6d_truncated(self.dist, newbunch, self.means, self.bunch_covar, self.limits)
            elif opts.linac_beam == "pencil":
                # pencil beam has all particles at center
                local_particles = newbunch.get_local_particles()
                local_particles[:,0:6] = 0.0
            elif opts.linac_beam == "cdr":
                synergia.bunch.populate_6d(self.dist, newbunch, self.means, self.bunch_covar)

            local_particles = newbunch.get_local_particles()
            # bunchlet_times are in tune units while particle coordinates
            # are in units of c*t
            local_particles[:, 4] = synergia.foundation.pconstants.c * btime

            # Injection position at elevation of 113 mm
            local_particles[:, 2] += 0.113

            # offset momentum and x position if injecting off-momentum
            if not opts.on_momentum:
                local_particles[:, 5] += 7.0e-4
                # have to shift the injection point to the outside of the
                # ring which is +x in MadX coordinates
                local_particles[:, 0] += +0.0015

            #print("adding new bunchlet at time: ", btime)
            bunch.inject(newbunch)

        #print("returning bunch, total particles: ", bunch.get_total_num())
        return bunch

########################################################################

nnodes = os.getenv('SLURM_NNODES')
nodeid = os.getenv('SLURM_NODEID')
localid = os.getenv('SLURM_LOCALID')
procid = os.getenv('SLURM_PROCID')
nprocs = os.getenv('SLURM_NPROCS')

print('Proc: {}/{}, running in process: {} on node: {}/{}'.format(procid, nprocs, localid, nodeid, nnodes))

logger = synergia.utils.Logger(0, "logger.out")

# create the lattice with default parameters.  This will be modified
# during the injection process.

# Inject along an ellipse
painting = painting.Painting()

if opts.injection:
    ORB_scale = 1
    xpos, ypos = painting.pos(opts.injection)
    HP_scale, VP_scale = painting.turn_to_hvpscale(opts.injection)
    print('    injection position: {:.8f}, {:.8f}'.format(xpos, ypos), file=logger)
else:
    ORB_scale = 1
    HP_scale = 0
    VP_scale = 1

print('    HP_scale: {:.7f}, VP_scale: {:.7f}'.format(HP_scale, VP_scale), file=logger)

lattice_madx = booster_lattice2.read_booster(ORB_scale=ORB_scale, HP_scale=HP_scale, VP_scale=VP_scale, writefile=True)

reader = synergia.lattice.MadX_reader()
reader.parse(lattice_madx)
orig_lattice = reader.get_lattice('booster_foil')
del reader
lattice = fixup_elements(orig_lattice, dogs_to_drifts=False)

mp = synergia.foundation.pconstants.mp # proton mass

# turn on the apertures if requested

if opts.apertures:
    print('Apertures ON', file=logger)
    # turn off injection region first
    unmark_injection_region(lattice)
    # then turn on all appropriate apertures
    set_apertures(lattice)
else:
    print('Apertures OFF', file=logger)


if opts.chef_propagate:
    print("Using CHEF to propagate", file=logger)
    use_chef(lattice)
elif opts.libff:
    print("using libff to propagate", file=logger)
    use_libff(lattice)
elif opts.libff_norf:
    print("using libff to propagate except RF cavities", file=logger)
    use_libff_except_rf(lattice)
elif opts.chef_map:
    print("using chef maps to propagate", file=logger)
    use_maps(lattice)
else:
    print("Using default to propagate", file=logger)

print("lattice length: ", lattice.get_length(), file=logger)
print("lattice total angle: ", lattice.get_total_angle(), file=logger)

refpart = lattice.get_reference_particle()
energy = refpart.get_total_energy()
momentum = refpart.get_momentum()
gamma = refpart.get_gamma()
beta = refpart.get_beta()

print("beam energy: ", energy, file=logger)
print("beam momentum: ", momentum, file=logger)
print("gamma: ", gamma, file=logger)
print("beta: ", beta, file=logger)

set_rfcavity_voltage(lattice, opts.rf_voltage)

commxx = synergia.utils.Commxx()
myrank = commxx.get_rank()
mpisize = commxx.get_size()

if myrank == 0:
    f = open("booster_lattice.txt", "w")
    print(lattice.as_string(), file=f)
    f.close()
    synergia.utils.write_lsexpr_file(lattice.as_lsexpr(), "booster.lsx")

# make an independent stepper for calculations of lattice functions and
# RF frequency
dummy_stepper = synergia.simulation.Independent_stepper(lattice, 1, opts.steps)    
lattice_simulator = dummy_stepper.get_lattice_simulator()

chef_beamline = lattice_simulator.get_chef_lattice().get_beamline()
f = open("booster_beamline.txt", 'w')
print(synergia.lattice.chef_beamline_as_string(chef_beamline),file=f)
f.close()

# extract useful parameters
lattice_simulator.register_closed_orbit()
closed_orbit = lattice_simulator.get_closed_orbit()
closed_orbit_length = lattice_simulator.get_closed_orbit_length()

# register_closed_orbit() works for CHEF.  For libff, we need to do
dummy_stepper.get_lattice_simulator().tune_circular_lattice(1.0e-6)

# register closed orbit/tune_circular_lattice sets the RF cavity frequencies
rf_frequency = lattice_simulator.get_rf_frequency()

print("closed orbit length: ", closed_orbit_length, file=logger)
print("closed orbit: ", closed_orbit, file=logger)
print("RF frequency: ", rf_frequency, file=logger)

bucket_length = beta * synergia.foundation.pconstants.c/rf_frequency
print("Bucket length: ", bucket_length, file=logger)

if myrank == 0:
    f = open("booster_lattice_after_tune.txt", "w")
    print(lattice.as_string(), file=f)
    f.close()
    synergia.utils.write_lsexpr_file(lattice.as_lsexpr(), "tuned_booster.lsx")

    # # save element names and types for offline programs
    # elems = []
    # s = 0.0
    # for e in lattice.get_elements():
    #     elems.append((e.get_name(), e_get_type(), s, e.get_length()))
    #     s = s + e.get_length()

    # with f = open('elems.pickle', 'wb') as f:
    #     pickle.save("elems.npy", f)

map = lattice_simulator.get_linear_one_turn_map()
print("one turn map", file=logger)
print(np.array2string(map, max_line_width=200), file=logger)
l, v = np.linalg.eig(map)
print("eigenvalues: ", file=logger)
for z in l:
    print("|z|: ", abs(z), " z: ", z, " tune: ", np.log(z).imag/(2.0*np.pi), file=logger)

# create the bunch
bunch = synergia.bunch.Bunch(refpart, 4, 0.0, commxx)
bunch.set_z_period_length(bucket_length)
bunch.set_bucket_index(0)

particles = bunch.get_local_particles()
particles[0, 0:6] = 0.0

particles[1, 2] = 0.113


# create the loss diagnostics
aperture_diag = synergia.lattice.Diagnostics_loss("aperture_diagnostics.h5", "aperture")
aperture_diag.set_bunch(bunch)
zcut_diag = synergia.lattice.Diagnostics_loss("zcut_diagnostics.h5", "zcut")
zcut_diag.set_bunch(bunch)

lattice.add_loss_diagnostics(aperture_diag)
lattice.add_loss_diagnostics(zcut_diag)

# Now create the stepper
stepper = synergia.simulation.Independent_stepper_elements(lattice, 1, 1)

bunch_simulator = synergia.simulation.Bunch_simulator(bunch)


bunch_simulator.add_per_turn(synergia.bunch.Diagnostics_full2("diag.h5"))
bunch_simulator.add_per_step(synergia.bunch.Diagnostics_bulk_track("tracks.h5", 2))

propagator = synergia.simulation.Propagator(stepper)
propagator.set_checkpoint_period(opts.checkpointperiod)

propagator.propagate(bunch_simulator, 1, 1, opts.verbosity)
