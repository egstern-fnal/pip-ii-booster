#!/usr/bin/bash

# found a bunch of options to help with display
# apparently pixel format needs to be yuv420p to play correctly
# if you to slow down by factor of 3, add video filter setpts=3.0*PTS


FFMPEG=/work1/accelsim/egstern/ffmpeg/ffmpeg
${FFMPEG} -i x-y-%04d.png -filter:v format=yuv420p -c:v libx264 -preset slow -profile:v main -crf 20 x-y-squeeze.mp4

${FFMPEG} -i x-xp-%04d.png -filter:v format=yuv420p -c:v libx264 -preset slow -profile:v main -crf 20 x-xp-squeeze.mp4

${FFMPEG} -i y-yp-%04d.png -filter:v format=yuv420p -c:v libx264 -preset slow -profile:v main -crf 20 y-yp-squeeze.mp4

${FFMPEG} -i z-zp-%04d.png -filter:v format=yuv420p -c:v libx264 -preset slow -profile:v main -crf 20 z-zp-squeeze.mp4
