#!/usr/bin/env python3
import sys, os
import numpy as np
import matplotlib.pyplot as plt

fmag_vertices = [(3.74,0.506), (-2.16, +1.09), (-2.16, -1.09), (3.74, -0.506)]
dmag_vertices = [(3.50,1.52), (-2.40, +0.901), (-2.40, -0.901), (3.50, -1.52)]

# duplicate beginning point
fmag_vertices.append(fmag_vertices[0])
dmag_vertices.append(dmag_vertices[0])

fmag_vertices = np.array(fmag_vertices)*0.0254
dmag_vertices = np.array(dmag_vertices)*0.0254

pipesizex = (np.array(fmag_vertices)[:,0].max() -
             np.array(fmag_vertices)[:,0].min())
pipesizey = (np.array(dmag_vertices)[:,1].max() -
                      np.array(dmag_vertices)[:,1].min())

plt.plot(fmag_vertices[:,0], fmag_vertices[:, 1], '-', label='F magnet')
plt.plot(dmag_vertices[:,0], dmag_vertices[:, 1], '-', label='D magnet')

rect_boundary = np.array( [ [pipesizex/2, pipesizey/2], [-pipesizex/2, pipesizey/2], [-pipesizex/2, -pipesizey/2], [pipesizex/2, -pipesizey/2], [pipesizex/2, pipesizey/2] ] )

plt.plot(rect_boundary[:,0], rect_boundary[:, 1], '-', lw=2, label='SC boundary')

xlim = plt.gca().get_xlim()
ylim = plt.gca().get_ylim()

plt.grid()

plt.legend(loc='best')

plt.savefig('boundary.png')
plt.savefig('boundary.eps')
plt.savefig('boundary.svg')

plt.show()
