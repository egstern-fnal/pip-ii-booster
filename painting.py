#!/usr/bin/env python3
import sys, os
import numpy as np
from numpy import sin, cos, pi
import matplotlib.pyplot as plt

class Painting:
    a = -6.1e-3 # from Pozdeyev. The document specifies +x is towards the
                # inside of the ring but MadX coordinates have -x is towards
                # the inside.
    b = -11.0e-3 # from # Pozdeyev
    phi_i = 7.2*pi/180.0 # initial painting phase 7.2 degrees from Pozdeyev
    N = 285 # number of painting turns from Pozdeyev
    max_vp = 0.1131905954 # max vertical position from VP_scale=1
    max_vpscale=1.0
    min_vp = 0.1020511418 # min vertical position from VP_scale=0.312
    min_vpscale = 0.312
    
    max_hp = 0.005035997 # max horizontal position from HP_scale=1
    max_hpscale = 1.0
    min_hp = 0.0 # min horizontal position from HP_scale = 0
    min_hpscale=0

    # max_vp will correspond to the central orbit height of 0.0

    # return (x, y) for central painting position for turn number
    def pos(self, turnnum):
        if turnnum < 0:
            raise RuntimeError('invalid turnnum < 0')
        if turnnum > Painting.N:
            raise RuntimeError('invalid turnnum > {}'.format(Painting.N))
        # this expression from Pozdeyev  "20210817 simulation parameters.pptx"
        # slide 10
        phi = (np.pi/2 - 2*Painting.phi_i)*turnnum/Painting.N + Painting.phi_i
        x = Painting.a*cos(phi)
        y = Painting.b*sin(phi)
        return (x,y)
    
    # calculate the VP_scale parameter needed to achieve the central orbit
    # y position for injected particles
    def y_to_vpscale(self, y):
        # Full VP_scale of 1 corresponds to max height at the injection foil
        # of max_vp=0.113, but that will become 0 when VP_scale and ORB_scale relax
        # to their post-painting values
        y = y + Painting.max_vp
        vp_scale = Painting.min_vpscale + (y-Painting.min_vp)*(Painting.max_vpscale-Painting.min_vpscale)/(Painting.max_vp - Painting.min_vp)
        return vp_scale

    def x_to_hpscale(self, x):
        # Full VP_scale of 1 corresponds to horizontal offset at the
        # injection foil 5.03mm.
        hp_scale = Painting.min_hpscale + x*(Painting.max_hpscale - Painting.min_hpscale)/(Painting.max_hp - Painting.min_hp)
        return hp_scale

    # return hp_scale and vp_scale for particular injection turn
    def turn_to_hvpscale(self, turnnum):
        x, y = Painting.pos(self, turnnum)
        hps = Painting.x_to_hpscale(self, x)
        vps = Painting.y_to_vpscale(self, y)
        return (hps, vps)

if __name__ == "__main__":
    p = Painting()

    print('Initial position: ', p.pos(0))
    print('Final position: ', p.pos(Painting.N))

    turns = list(range(p.N+1))
    x = np.array([p.pos(t)[0] for t in turns])
    y = np.array([p.pos(t)[1] for t in turns])

    plt.figure()
    plt.title('x')
    plt.plot(x)

    plt.figure()
    plt.title('y')
    plt.plot(y)

    plt.figure()
    plt.title('y-x')
    plt.plot(x, y)
    plt.xlabel('x')
    plt.ylabel('y')

    print('horizontal displacement: ', Painting.a*(cos(np.pi/2-Painting.phi_i) - cos(Painting.phi_i)))
    print('vertical displacement: ', Painting.b*(sin(np.pi/2-Painting.phi_i) - sin(Painting.phi_i)))

    plt.show()
