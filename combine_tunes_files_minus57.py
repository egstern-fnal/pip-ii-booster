#!/usr/bin/env python3
import sys, os
import numpy as np
import glob

alltunes = {}
for f in glob.glob('tracks_ud_0[0-9][0-9][0-9]_tunes.npy'):
    print('loading file ', f, end='')
    t = np.load(f, allow_pickle=True)[()]
    # for each particle, find the tune corresponding to the track starting at
    # track start index -57.  Add that one into alltunes.

    fcnt = 0
    for pid in t:
        ts = t[pid][0][-57]
        #print('ts: ', ts)
        #print('pid: ', pid, ' track starts at ', ts)
        xtune = t[pid][1][-57]
        #print('xtune: ', xtune)
        ytune = t[pid][2][-57]
        #print('ytune: ', ytune)
        ztune = t[pid][3][-57]
        #print('ztune: ', ztune)
        fcnt = fcnt + 1
        alltunes[pid] = ([ts], [xtune], [ytune], [ztune])

    print(' read ', fcnt, ' track tunes')
    del ztune, ytune, xtune, ts
    del t

np.save('alltunes.npy', alltunes)
