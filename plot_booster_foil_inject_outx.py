#!/usr/bin/env python3
import sys,os
import numpy as np
import matplotlib.pyplot as plt

mdx_dir = 'pip-ii-booster-madx-files'


s,l,k1l,k2l,y,py,x,px,betx,bety,alfx,alfy,mux,muy,dx,dpx,dy,dpy,dispx,dispy = np.loadtxt(mdx_dir + '/booster_foil_inject.outx', skiprows=48, usecols=(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20),unpack=True)

# still need to get the names of elements
f = open(mdx_dir + '/booster_foil.outx', 'r')
outxlines = f.readlines()[48:]
f.close()
booster_names = [ l.split()[0] for l in outxlines]

print('len(booster_names): ', len(booster_names))
print('len(mux): ', len(mux))

for i in range(len(mux)-1):
    print('{}\t\t{}\t{}'.format(booster_names[i],  (mux[i+1]-mux[i])*360, (muy[i+1]-muy[i])*360))


plt.figure()
plt.title('k1l')
plt.plot(s,k1l)

plt.figure()
plt.title('k2l')
plt.plot(s,k2l)

plt.figure()
plt.title('y')
plt.plot(s,y)

plt.figure()
plt.title('py')
plt.plot(s,py)

plt.figure()
plt.title('x')
plt.plot(s,x)

plt.figure()
plt.title('px')
plt.plot(s,px)

plt.figure()
plt.title('betx, bety')
plt.plot(s,betx, label='betx')
plt.plot(s,bety, label='bety')
plt.legend(loc='best')

plt.figure()
plt.title('alfx, alfy')
plt.plot(s,alfx, label='alfx')
plt.plot(s,alfy, label='alfy')
plt.legend(loc='best')

plt.figure()
plt.title('mux, muy')
plt.plot(s,mux, label='mux')
plt.plot(s,muy, label='muy')
plt.legend(loc='best')

plt.figure()
plt.title('dx, dy')
plt.plot(s,dx,label='dx')
plt.plot(s,dy, label='dy')
plt.legend(loc='best')

plt.figure()
plt.title('dpx, dpy')
plt.plot(s,dpx, label='dpx')
plt.plot(s,dpy, label='dpy')
plt.legend(loc='best')

plt.figure()
plt.title('dispx and dispy')
plt.plot(s,dispx,label='dispx')
plt.plot(s,dispy,label='dispy')
plt.legend(loc='best')

plt.show()

