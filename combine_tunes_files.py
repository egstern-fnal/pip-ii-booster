#!/usr/bin/env python3
import sys, os
import numpy as np
import glob

alltunes = {}
for f in glob.glob('tracks_0[0-9][0-9][0-9]_tunes.npy'):
    print('loading file ', f)
    t = np.load(f, allow_pickle=True)[()]
    alltunes.update(t)
    del t

np.save('alltunes.npy', alltunes)
