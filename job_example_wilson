#!/bin/bash
#SBATCH -N @@numnode@@
__queue{{#SBATCH -p @@queue@@}}{{}}__
#SBATCH -q regular
#SBATCH --mail-user=egstern@fnal.gov
#SBATCH --mail-type=ALL
__walltime{{#SBATCH -t @@walltime@@}}{{}}__
__account{{#SBATCH -A @@account@@}}{{}}__
###SBATCH -J foo you can set a job name by uncommenting this line and giving an argument to -J
#SBATCH --output=synergia-%j.out
#SBATCH --error=synergia-%j.err

HOME=/work1/accelsim/egstern

#2020-11-11 EGS Wilson cluster migrated to wc.

# 2018-05-09 Wilson cluster batch system changed to SLURM
# This script is an appropriate template for running SLURM jobs on the
# wilson cluster.

# The following line is needed to use submit=1
#synergia_workflow_submitter:sbatch

#OpenMP settings:
export OMP_NUM_THREADS=1
export OMP_PLACES=threads
export OMP_PROC_BIND=spread
export SYNERGIA2DIR=@@synergia2dir@@

echo "process ${SLURM_PROCID}/${SLURM_NPROCS}, local proc ${SLURM_LOCALID} on node ${SLURM_NODEID}/${SLURM_NNODES}"

# sets up modules and paths
. @@setupsh@@

echo "PATH: ${PATH}"
echo
echo "LD_LIBRARY_PATH: ${LD_LIBRARY_PATH}"
echo
echo "PYTHONPATH: ${PYTHONPATH}"

# just in case you're reading a foreign HDF5 file.
export HDF5_DISABLE_VERSION_CHECK=2

echo "Job nodes: " $SLURM_JOB_NODELIST

cores_per_box=16

# logical cores/MPI task calculation
cores_per_mpi=$(( @@numnode@@ * $cores_per_box/@@numproc@@ ))

echo "Job start at `date`"
# suppress openmpi fork() warning messages
export OMPI_MCA_mpi_warn_on_fork=0
echo "synergia is: " `which synergia`

#srun --ntasks=@@numproc@@ --ntasks-per-node=@@procspernode@@ --mpi=pmix --cpu_bind=cores @@synergia_executable@@ @@script@@ @@args@@

#export LD_PRELOAD=/opt/ohpc/pub/libs/gnu8/openmpi3/fftw/3.3.8/lib/libfftw3.so

srun --ntasks=@@numproc@@ --ntasks-per-node=@@procspernode@@ --mpi=pmix --cpu_bind=cores python3 @@script@@ @@args@@

echo "Job end at `date`"
