#!/usr/bin/env python3
import sys, os
import numpy as np
import synergia

refpart = synergia.foundation.Reference_particle(1, synergia.foundation.pconstants.mp, synergia.foundation.pconstants.mp+0.8)
linac_covar = np.load('linac_covariance.npy')

commxx = synergia.utils.Commxx()

bunch = synergia.bunch.Bunch(refpart, 1000000, 1.0e11, commxx)

seed = 4
distr = synergia.foundation.Random_distribution(seed, commxx)

linac_means = np.zeros(6)
bunch_limits = np.array([4.0, 4.0, 4.0, 4.0, 2.5, 4.0])
# void
# populate_6d_truncated(Distribution &dist, Bunch &bunch,
#         Const_MArray1d_ref means, Const_MArray2d_ref covariances,
#         Const_MArray1d_ref limits)
synergia.bunch.populate_6d_truncated(distr, bunch, linac_means, linac_covar, bunch_limits)

part = synergia.bunch.Diagnostics_particles('particles.h5')
part.set_bunch(bunch)
part.update_and_write()


