#!/usr/bin/bash

#synbeamplot nfparticles_0012.h5 --minh=-2.0e-7 --maxh=2.0e-7 --minv=-2.0e-7 --maxv=2.0e-7 --bins=40 y yp

#nnnn="0000 0001 0002 0003 0004 0005 0006 0007 0008 0009 0010 0011 0012 0013"
#nnnn=`ls particles_????.h5 | sed -e 's/particles_\([0-9]\{4\}\).h5/\1/'`

if env | grep -iq slurm
then
    mpisize=$SLURM_NTASKS
    myrank=$SLURM_PROCID
else
    mpisize=1
    myrank=0
fi

nfiles=`ls inj_particles_????.h5 | wc -l`
files_per_proc=$(( $nfiles / $mpisize + 1 ))
myfirst=$(( $files_per_proc * $myrank ))
mylast=$(( $myfirst + $files_per_proc ))
if [ $mylast -gt $nfiles ]
then
    mylast=$nfiles
fi

export MPLCONFIGDIR=/work1/accelsim/egstern/.matplotlib

beamplot="/work1/accelsim/egstern/syn2-devel-pre3/install/lib/synergia_tools/beam_plot.py"

for ((i=myfirst; i<mylast; ++i ))
do
    f=$(printf "%04d" $i)
    filename="inj_particles_${f}.h5"
    echo "file number: $f, plotting file $filename"

#CDR run
#Global mins:  [-1.43121521e-02 -2.00630794e-03  8.38404614e-02 -9.72204089e-04
# -3.35131390e+00 -2.18388456e-03]
#Global maxs:  [2.12327746e-02 2.00439931e-03 1.23766836e-01 9.80141508e-04
# 3.35158584e+00 2.32964187e-03]

# Realistic run
#Global mins:  [-1.33943857e-02 -1.94372212e-03  8.21846420e-02 -1.06151060e-03
# -2.24332525e+00 -1.89678135e-03]
#Global maxs:  [2.07401460e-02 1.94356771e-03 1.24565492e-01 1.04780381e-03


# 2.23898130e+00 1.89662123e-03]


# x xp
#    MPLBACKEND=Agg python $beamplot $filename --minh=-0.015 --maxh=0.015 --minv=-0.002 --maxv=0.002 --bins=40 --contour=25 --output=x-xp-$f.png x xp
#    MPLBACKEND=Agg python $beamplot $filename --minh=-0.095 --maxh=0.095 --minv=-0.002 --maxv=0.002 --bins=40 --contour=25 --output=x-xp-$f.png x xp

# this was previous painting
#    MPLBACKEND=Agg python $beamplot $filename --minh=-0.018 --maxh=0.018 --minv=-0.0025 --maxv=0.0025 --bins=40 --contour=25 --output=x-xp-$f.png x xp
    #XXPLIM="--minh=-1.5e-2 --maxh=1.5e-2 --minv=-1.1e-3 --maxv=1.1e-3"
    XXPLIM="--minh=-2.2e-2 --maxh=2.2e-2 --minv=-2.1e-3 --maxv=2.1e-3"
    MPLBACKEND=Agg python $beamplot $filename  --bins=50 --contour=10 ${XXPLIM} --output=x-xp-$f.png x xp

# y yp
#    MPLBACKEND=Agg python $beamplot $filename --minh=-0.022 --maxh=0.022 --minv=-0.0011 --maxv=0.0011 --bins=40 --contour=25 --output=y-yp-$f.png y yp
#    MPLBACKEND=Agg python $beamplot $filename --minh=-0.039 --maxh=0.039 --minv=-0.0011 --maxv=0.0011 --bins=40 --contour=25 --output=y-yp-$f.png y yp

# this was previous pain
#    MPLBACKEND=Agg python $beamplot $filename --minh=-0.025 --maxh=0.025 --minv=-0.0015 --maxv=0.0015 --bins=40 --contour=25 --output=y-yp-$f.png y yp

    #YYPLIM="--minh=8.9e-2 --maxh=1.24e-1 --minv=-7e-4 --maxv=7e-4"
    YYPLIM="--minh=8.3e-2 --maxh=1.24e-1 --minv=-9.9e-4 --maxv=9.9e-4"
    MPLBACKEND=Agg python $beamplot $filename ${YYPLIM} --bins=50 --contour=10 --output=y-yp-$f.png y yp

#    MPLBACKEND=Agg python $beamplot $filename --minh=-0.015 --maxh=0.015 --minv=-0.022 --maxv=0.022 --bins=40 --contour=25 --output=x-y-$f.png x y
#    MPLBACKEND=Agg python $beamplot $filename --minh=-0.095 --maxh=0.095 --minv=-0.039 --maxv=0.039 --bins=40 --contour=25 --output=x-y-$f.png x y

# this was previous painting
#    MPLBACKEND=Agg python $beamplot $filename --minh=-0.018 --maxh=0.018 --minv=-0.025 --maxv=0.025 --bins=40 --contour=25 --output=x-y-$f.png x y
    #XYLIM="--minh=-1.5e-2 --maxh=1.5e-2 --minv=8.9e-2 --maxv=1.24e-1"
    XYLIM="--minh=-2.2e-2 --maxh=2.2e-2 --minv=8.3e-2 --maxv=1.24e-1"
    MPLBACKEND=Agg python $beamplot $filename  --bins=50 $XYLIM --contour=10 --output=x-y-$f.png x y

#    MPLBACKEND=Agg python $beamplot $filename --minh=-3 --maxh=3 --minv=-0.0022 --maxv=0.0022 --bins=40 --contour=25 --output=z-zp-$f.png z zp
#    MPLBACKEND=Agg python $beamplot $filename --minh=-3 --maxh=3 --minv=-0.0022 --maxv=0.0022 --bins=50 --contour=25 --output=z-zp-$f.png z zp

    ZDPOPLIM="--minh=-2.8 --maxh=2.8 --minv=-2.4e-3 --maxv=2.4e-3"
    MPLBACKEND=Agg python $beamplot $filename ${ZDPOPLIM} --bins=50 --contour=10 --output=z-zp-$f.png z dpop

done
