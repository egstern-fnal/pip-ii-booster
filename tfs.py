# tfs class to read TFS files as produced by MAD-X and other CERN programs
#
from __future__ import print_function
import numpy as np
import re

########################################################################

def _extract_descriptors(tfslines):
    linekeys = [tl[0] for tl in tfslines]
    descriptors = {}
    vnames = []
    vvals = []
    # the descriptors are all at the beginning of the file.
    for k, kline in zip(linekeys, tfslines):
        if k != '@':
            # this is not a descriptor line, so no more descriptors here
            break
        # typical lines are:
        # @ SEQUENCE         %05s "MODEL"
        # @ PARTICLE         %06s "PROTON"
        # @ MASS             %le         0.938272013
        # @ CHARGE           %le                   1
        # first word after @ is the name, second word is type, after that
        #   is the value
        ksplit = kline[1:].split()
        vnames.append(ksplit[0].lower())
        vtype = ksplit[1]
        vv = " ".join(ksplit[2:])
        if vtype == "%le":
            vvals.append(float(vv))
        elif re.match("%\d+s", vtype):
            vvals.append(vv.replace('"', ''))

    for kname, kval in zip(vnames, vvals):
        descriptors[kname] = kval

    return descriptors

########################################################################

def _extract_colnames(tfslines):
    linekeys = [tl[0] for tl in tfslines]
    # column names begin with the * character
    name_line_idx = linekeys.index('*')
    colnames = [x.lower() for x in tfslines[name_line_idx][1:].split()]
    return colnames

########################################################################

def _extract_data(tfslines):
    linekeys = [tl[0] for tl in tfslines]
    # data lines are all those after the first line not beginning with
    # a @ * $
    first_data = -1
    for k, c in enumerate(linekeys):
        if (c != '@') and (c != '*') and (c != '$'):
            first_data = k
            break
    if first_data < 0:
        raise RuntimeError('no data lines in this file')
    # check that all remaining data lines contain the same number of words
    nwords = [len(ll.split()) for ll in tfslines[first_data:]]
    n = nwords[0]
    if nwords.count(n) != len(nwords):
        raise RuntimeError('data lines do not all contain the same number of entries')
    nrows = len(tfslines) - first_data
    data = np.zeros((nrows, n), 'd')
    for r, ll in enumerate(tfslines[first_data:]):
        data[r, :] = np.array([float(x) for x in ll.split()])
    return data

########################################################################

class Tfs:
            
    def __init__(self, tfsfile):
        tfsfo = open(tfsfile)
        tfslines = tfsfo.readlines()
        tfsfo.close()
        self._descriptors = _extract_descriptors(tfslines)
        self._colnames = _extract_colnames(tfslines)
        self._data = _extract_data(tfslines)
        return

    def _colname_to_idx(self, colname):
        idx = self._colnames.index(colname.lower())
        return idx

    def get_colnames(self):
        return self._colnames

    # row is one-based in MADX convention
    def get_row(self, row):
        return self._data[row-1, :]

    # get the time-series data for a named column
    def get_coldata(self, name):
        idx = self._colname_to_idx(name)
        return self._data[:, idx]

    # row is one-based in MADX convention
    def get_name_value(self, row, name):
        idx = self._colname_to_idx(name)
        return self._data[row-1, idx]

    # row is one-based in MADX convention
    # print out madx commands to define the column variables names with
    # values from a specific row.
    def define_row_variables(self, row):
        setvar_commands = ''
        for k, nm in enumerate(self._colnames):
            setvar_commands = setvar_commands + '{:s} = {:.16g};\n'.format(nm, self._data[row-1, k])
        return setvar_commands
            
