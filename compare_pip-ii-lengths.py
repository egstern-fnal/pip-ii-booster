#!/usr/bin/env python3
import sys,os
import numpy as np
import matplotlib.pyplot as plt

mdx_dir = 'pip-ii-booster-madx-files'


# madx inserts BOOSTER$START and $BOOSTER$END markers
s0,l0,angle,x,y,z,theta,phi,psi,globaltilt = np.loadtxt(mdx_dir + '/survey_pip2.outx', skiprows=8, usecols=(2,3,4,5,6,7,8,9,10,11),unpack=True)
s0 = s0[1:-1]
madxf = open(mdx_dir + '/survey_pip2.outx', 'r')
madxlines = madxf.readlines()[8:]
madxf.close()

madxnames = [l.split()[0].lower().replace('"', '')  for l in madxlines[1:-1]]
print('first 10 madx names: ')
print(madxnames[0:10])
print('last 10 madx names: ')
print(madxnames[-10:])

s1,l1 = np.loadtxt('pip-ii-lattice-lengths.out', usecols=(2,3), unpack=True)
synf = open('pip-ii-lattice-lengths.out', 'r')
synlines = synf.readlines()
synf.close()
synnames = [l.split()[0] for l in synlines]
print('first 10 synnames:')
print(synnames[:10])
print('last 10 synnames:')
print(synnames[-10:])

print('len(s0): ', len(s0))
print('len(s1): ', len(s1))
print()
print('len(l0): ', len(l0))
print('len(l1): ', len(l1))

f = open('madxnames.out', 'w')
[ print(foo,file=f) for foo in madxnames ]
f.close()

f = open('synnames.out', 'w')
[ print(foo, file=f) for foo in synnames ]
f.close()

for i in range(len(madxnames)):
    if synnames[i] != madxnames[i]:
        print('synname: {} != madxname: {} at line {}'.format(synnames[i], madxnames[i], i))
        break
