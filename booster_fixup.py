#!/usr/bin/env python3
from __future__ import print_function

import sys, os
import numpy as np
import synergia

DEBUG = False

########################################################################

# fix up elements
def fixup_elements(orig_lattice, rbends_to_sbends=True, zero_angle_bends_to_drifts=True, dogs_to_drifts=True, kill_sept03=True):

    if callable(hasattr(orig_lattice, 'get_element_adaptor_map_sptr')):
        lattice = synergia.lattice.Lattice(orig_lattice.get_name(), orig_lattice.get_element_adaptor_map_sptr())
    else:
        lattice = synergia.lattice.Lattice(orig_lattice.get_name())


    for elem in orig_lattice.get_elements():

        new_elem_here = False

        if zero_angle_bends_to_drifts and (elem.get_type() == "rbend" or elem.get_type() == "sbend"):
            angle = 0.0
            if elem.has_double_attribute("angle"):
                angle = elem.get_double_attribute("angle")
            if abs(angle) < 1.0e-8 :
                new_elem = synergia.lattice.Lattice_element("drift", elem.get_name())
                new_elem.set_double_attribute("l", elem.get_length())
                new_elem_here = True
                if DEBUG:
                    print("old elem: ", elem.as_string())
                    print("new elem: ", new_elem.as_string())
                elem = new_elem

        if dogs_to_drifts and elem.get_name()[0:3] == "dog":
            new_elem = synergia.lattice.Lattice_element("drift", elem.get_name())
            s_attributes = elem.get_string_attributes()
            d_attributes = elem.get_double_attributes()
            for s in s_attributes.keys():
                new_elem.set_string_attribute(s, s_attributes[s])
            for d in d_attributes.keys():
                if (d=='l'):
                    new_elem.set_double_attribute(d, d_attributes[d])
            if DEBUG:
                print("old dog: ", elem.as_string())
                print("new dog: ", new_elem.as_string())
            elem = new_elem

        if kill_sept03 and elem.get_name() == "sept03":
            new_elem = synergia.lattice.Lattice_element("drift", "sept03")
            new_elem.set_double_attribute("l", elem.get_length())
            elem = new_elem

        if rbends_to_sbends and elem.get_type() == "rbend":
            new_elem_here = True
            new_elem = synergia.lattice.Lattice_element("sbend", elem.get_name())
            s_attributes = elem.get_string_attributes()
            d_attributes = elem.get_double_attributes()
            for s in s_attributes.keys():
                new_elem.set_string_attribute(s, s_attributes[s])
            for d in d_attributes.keys():
                new_elem.set_double_attribute(d, d_attributes[d])
            ang = 0.0
            if elem.has_double_attribute("angle"):
                ang = elem.get_double_attribute("angle")
            length = elem.get_double_attribute("l")
            arclength = ang*length/(2.0*np.sin(ang/2.0))
            new_elem.set_double_attribute("l", arclength)
            new_elem.set_double_attribute("e1", ang/2.0)
            new_elem.set_double_attribute("e2", ang/2.0)
            if DEBUG:
                print("old rbend: ", elem.as_string())
                print("new sbend: ", new_elem.as_string())
            elem = new_elem

        lattice.append(elem)

    # end loop over elem in orig_lattice
    lattice.set_reference_particle(orig_lattice.get_reference_particle())
    return lattice

########################################################################

# set the RF cavity voltage
def set_rfcavity_voltage(lattice, total_voltage):
    rfcnt = 0
    for elem in lattice.get_elements():
        if elem.get_type() == "rfcavity":
            rfcnt = rfcnt + 1
            #print(elem.as_string())                                                       #print("Found {} RF cavities".format(rfcnt))

    for elem in lattice.get_elements():
        if elem.get_type() == "rfcavity":
            elem.set_double_attribute("volt", total_voltage/rfcnt)

########################################################################

def kill_muldg(lattice):
    for elem in lattice.get_elements():
        if elem.get_name()[0:5] == "muldg":
            elem.set_vector_attribute("knl", [0.0, 0.0, 0.0])

########################################################################

def use_maps(lattice):
    for elem in lattice.get_elements():
        elem.set_string_attribute("extractor_type", "chef_mixed")

########################################################################

def use_chef(lattice):
    for elem in lattice.get_elements():
        elem.set_string_attribute("extractor_type", "chef_propagate")

########################################################################

def use_libff(lattice):
    for elem in lattice.get_elements():
        elem.set_string_attribute("extractor_type", "libff")

########################################################################

def use_libff_except_rf(lattice):
    for elem in lattice.get_elements():
        if elem.get_type() != "rfcavity":
            elem.set_string_attribute("extractor_type", "libff")
        else:
            elem.set_string_attribute("extractor_type", "chef_propagate")

########################################################################
########################################################################
########################################################################
########################################################################

# mark BMA regions



#    On 8/3/20 12:06 PM, Salah J Chaurize wrote:
#    Short BMA min aperture is 4.5 inches.  long BMA min aperture is 3.125 inches,
#    dogleg BMA 3.875i#n and Orbump BMA is 4.375in. kickers ceramic beam tubes are 2.25 x 2.375in
#    RF cavity 2.25in. These numbers are mostly from mechanical drawings particularly for BMAs.
#    -Salah

#    From: Kiyomi Seiya <kiyomi@fnal.gov> 
#    Sent: Monday, August 3, 2020 10:27 AM
#    To: Cheng-Yang Tan <cytan@fnal.gov>
#    Cc: Salah J Chaurize <chaurize@fnal.gov>; David E Johnson <dej@fnal.gov>
#    Subject: RE: Do you have a map of Booster apertures

#    I checked my note.  The collimator aperture is 4.5 inch, and 5 inch for S12.
#    I don't recall short and long have different apertures.

#    Kiyomi

def mark_short_bma(lattice):
    mark_bma(lattice, 'hs%02d', 'sss%02d', 4.5 * 0.0254/2)

def mark_long_bma(lattice):
    mark_bma(lattice, 'hl%02d', 'ssl%02d', 3.125 * 0.0254/2)


# add a "short_bma" to string attribute aperture_type
def mark_bma(lattice, start_pattern, end_pattern, radius):
    elem_names = [e.get_name() for e in lattice.get_elements()]
    all_elems = list(lattice.get_elements())

    # sector goes from 1 to 24
    for sector in range(1,25):
        firstname = start_pattern%sector
        lastname = end_pattern%sector
        if firstname not in elem_names:
            #raise RuntimeError("%s not present in element names"%firstname)
            print('Error: {} element not found... skipping...'.format(firstname))
            continue

        firstloc = elem_names.index(firstname)
        
        if lastname not in elem_names:
            raise RuntimeError("%s not present in element names"%lastname)

        lastloc = elem_names.index(lastname)

        # search backwards from firstname for first nondrift
        nondrift = False
        startbma = None
        curloc = firstloc
        #print("short BMA ", sector, " search begin starting at ", curloc-1, all_elems[curloc-1].get_name(), ":", all_elems[curloc-1].get_type())
        while not nondrift:
            if all_elems[curloc-1].get_type() == "drift":
                curloc = curloc - 1
            else:
                nondrift = True

        bma_start = curloc

        #print("short BMA ", sector, " starts found at element ", curloc, all_elems[curloc].get_name(), ":", all_elems[curloc].get_type())
        
        nondrift = False
        endbma = None
        curloc = lastloc + 1
        while not nondrift:
            if all_elems[curloc].get_type() == "drift":
                curloc = curloc + 1
            else:
                nondrift = True
        #print("BMA ", sector, " ends at element", curloc, all_elems[curloc].get_name(),":", all_elems[curloc].get_type())

        bma_end = curloc

        for idx in range(bma_start, bma_end):
            all_elems[idx].set_string_attribute("aperture_type", "circular")
            all_elems[idx].set_double_attribute("circular_aperture_radius", radius)


########################################################################
fmag_vertices = [(3.74,0.506), (-2.16, +1.09), (-2.16, -1.09), (3.74, -0.506)]
dmag_vertices = [(3.50,1.52), (-2.40, +0.901), (-2.40, -0.901), (3.50, -1.52)]

def set_apertures_fdmag(lattice):
    for elem in lattice.get_elements():
        if elem.get_name()[0:4] == "fmag":
            # focussing magnet
            elem.set_string_attribute("aperture_type", "polygon")
            elem.set_double_attribute("the_number_of_vertices", 4)
            # vertex coordinates in inches
            vertices = fmag_vertices
            for i, vtx in enumerate(vertices):
                elem.set_double_attribute("pax%d"%(i+1), vtx[0]*0.0254)
                elem.set_double_attribute("pay%d"%(i+1), vtx[1]*0.0254)
            # find distance from center to aperture edge
            # need the equation for the between (-2.54, 0.925) to (3.46, 0.925)
            # line that goes between (x1, y1) and (x2, y2)
            #  A = (y2-y1), B = -(x2-x1), C = -x1*(y2-y1) + y1*(x2-x1)
            # min_radius2 = C**2/(A**2 + B**2)
            A = vertices[1][1] - vertices[0][1]
            B = -(vertices[1][0] - vertices[0][0])
            C = -vertices[0][0]*(vertices[1][1] - vertices[0][1]) + vertices[0][1]*(vertices[1][0] - vertices[0][0])
            elem.set_double_attribute("min_radius2", C**2/(A**2 + B**2))
        elif elem.get_name()[0:4] == "dmag":
            # defocussing magnet
            elem.set_string_attribute("aperture_type", "polygon")
            elem.set_double_attribute("the_number_of_vertices", 4)
            # vertex coordinates in inches
            vertices = dmag_vertices
            for i, vtx in enumerate(vertices):
                elem.set_double_attribute("pax%d"%(i+1), vtx[0]*0.0254)
                elem.set_double_attribute("pay%d"%(i+1), vtx[1]*0.0254)
            # find distance from center to aperture edge
            # need the equation for the between (-2.54, 0.925) to (3.46, 0.925)
            # line that goes between (x1, y1) and (x2, y2)
            #  A = (y2-y1), B = -(x2-x1), C = -x1*(y2-y1) + y1*(x2-x1)
            # min_radius2 = C**2/(A**2 + B**2)
            A = vertices[1][1] - vertices[0][1]
            B = -(vertices[1][0] - vertices[0][0])
            C = -vertices[0][0]*(vertices[1][1] - vertices[0][1]) + vertices[0][1]*(vertices[1][0] - vertices[0][0])
            elem.set_double_attribute("min_radius2", C**2/(A**2 + B**2))
            

########################################################################

def set_apertures(lattice):
    set_apertures_fdmag(lattice)
    mark_short_bma(lattice)
    mark_long_bma(lattice)
